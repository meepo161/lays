package ru.avem.lays

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.text.Font
import javafx.stage.Stage

class Main : Application() {
    override fun start(primaryStage: Stage) {
//        Font.loadFont(javaClass.getResource("styles/fonts/DSCrystal.ttf").toExternalForm(), 10.0)
        val root = FXMLLoader.load<Parent>(javaClass.getResource("layouts/mainView.fxml"))
        primaryStage.title = "AVEM Terminator"
        primaryStage.minWidth = 800.0
        primaryStage.minHeight = 400.0
        primaryStage.scene = Scene(root, 800.0, 400.0)
        primaryStage.show()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Application.launch(Main::class.java)
        }
    }
}
